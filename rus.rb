#!/usr/bin/env ruby
# frozen_string_literal: true

# rus allows to type Cyrillic characters from the Russian
# keyboard layout without the need to add a new layout or language in OS settings.

# This program works in an interactive mode when run without arguments.
# It also receives data on standard input and supports the `--rev` flag
# that reverses the mapping of characters.

require 'io/console'

eng_to_chr = {
  "\r" => "\n",
  "\c?" => "\x08"
}

eng_to_rus = {
  'a' => 'ф',
  'b' => 'и',
  'c' => 'с',
  'd' => 'в',
  'e' => 'у',
  'f' => 'а',
  'g' => 'п',
  'h' => 'р',
  'i' => 'ш',
  'j' => 'о',
  'k' => 'л',
  'l' => 'д',
  'm' => 'ь',
  'n' => 'т',
  'o' => 'щ',
  'p' => 'з',
  'q' => 'й',
  'r' => 'к',
  's' => 'ы',
  't' => 'е',
  'u' => 'г',
  'v' => 'м',
  'w' => 'ц',
  'x' => 'ч',
  'y' => 'н',
  'z' => 'я',
  ',' => 'б',
  '.' => 'ю',
  '[' => 'х',
  ']' => 'ъ',
  ';' => 'ж',
  '`' => 'ё',
  "'" => 'э',

  'A' => 'Ф',
  'B' => 'И',
  'C' => 'С',
  'D' => 'В',
  'E' => 'У',
  'F' => 'А',
  'G' => 'П',
  'H' => 'Р',
  'I' => 'Ш',
  'J' => 'О',
  'K' => 'Л',
  'L' => 'Д',
  'M' => 'Ь',
  'N' => 'Т',
  'O' => 'Щ',
  'P' => 'З',
  'Q' => 'Й',
  'R' => 'К',
  'S' => 'Ы',
  'T' => 'Е',
  'U' => 'Г',
  'V' => 'М',
  'W' => 'Ц',
  'X' => 'Ч',
  'Y' => 'Н',
  'Z' => 'Я',
  '<' => 'Б',
  '>' => 'Ю',
  '{' => 'Х',
  '}' => 'Ъ',
  ':' => 'Ж',
  '"' => 'Э',
  '~' => 'Ё',

  '@' => '"',
  '#' => '№',
  '$' => ';',
  '^' => ':',
  '/' => '.',
  '?' => ',',
  '&' => '?',
  "\s" => ' ',
  "\r" => "\n",
  "\c?" => "\x08"
}

def has_stdin?
  IO.select([$stdin], [], [], 0)
end

if has_stdin?
  input = $stdin.read
  mapping = ARGV.first == "--rev" ? eng_to_rus.invert : eng_to_rus
  puts input.chars.map { |ch| mapping.fetch(ch, ch) }.join
  exit 0
end

puts "Start typing, press '\\' to quit, press '|' to toggle English input."
puts

english = false

loop do
  ch = $stdin.getch
  break if ch == '\\'

  # Handle backspace.
  if ch == "\u007F"
    print("\b \b")
    next
  end

  if ch == '|'
    english = !english
    next
  end

  if english
    print(eng_to_chr.fetch(ch, ch))
    next
  end

  print(eng_to_rus.fetch(ch, ch))
end

puts
